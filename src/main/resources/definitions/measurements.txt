velocity * distance / time
acceleration * velocity / time
force * acceleration * mass
work * force * distance
power * work / time
momentum * velocity * mass
charge * amperage * time
area * distance * distance
volume * area * distance
density * mass / volume
pressure * force / area
wavenumber / distance
frequency / time
voltage * power / amperage
resistance * voltage / amperage