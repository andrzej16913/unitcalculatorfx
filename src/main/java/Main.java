package main.java;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Pair;
import main.java.controller.Controller;
import main.java.util.calculatorLogic.Calculator;
import main.java.util.calculatorLogic.Measurement;
import main.java.util.calculatorLogic.Word;
import main.java.util.fileReader.DataCorruptionException;
import main.java.util.fileReader.SymbolsReader;

import java.io.FileNotFoundException;
import java.util.Map;

public class Main extends Application {

    // Path definitions
    private String fxmlPath = "/main/resources/fxml/sample.fxml";
    private String measurementsPath = "/main/resources/definitions/measurements.txt";
    private String unitsPath = "/main/resources/definitions/units.txt";

    @Override
    public void start(Stage primaryStage) throws Exception{
        Calculator calculator = null;
        Map<Measurement, String> measurementStringMap = null;

        try {
            Pair<Map<String, Measurement>, Map<Measurement, String>> pair;
            pair = SymbolsReader.readMeasurements(measurementsPath);
            Map<String, Measurement> stringMeasurementMap = pair.getKey();
            measurementStringMap = pair.getValue();

            Map<String, Word> stringWordMap = SymbolsReader.readUnits(unitsPath, stringMeasurementMap);

            calculator = new Calculator(stringWordMap, 4);

        } catch (FileNotFoundException | DataCorruptionException e) {
            //output.setText(e.getMessage());
            //input.setEditable(false);
            //ans.setEditable(false);
            //calculator = null;

            System.out.println(e.getMessage());
        } finally {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(fxmlPath));
            final Calculator calc = calculator;
            final Map<Measurement, String> map = measurementStringMap;
            fxmlLoader.setControllerFactory(c -> new Controller(calc, map));
            Parent root = fxmlLoader.load();

            primaryStage.setTitle("Unit Calculator FX");
            primaryStage.setScene(new Scene(root, 600, 550));
            primaryStage.show();
            //primaryStage.show();
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}
