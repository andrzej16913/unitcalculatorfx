package main.java.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import main.java.util.calculatorLogic.Calculator;
import main.java.util.calculatorLogic.Measurement;
import main.java.util.calculatorLogic.Unit;
import main.java.util.calculatorLogic.exceptions.InvalidArithmeticException;
import main.java.util.calculatorLogic.exceptions.InvalidSyntaxException;

import java.util.Map;

public class Controller {

    private Calculator calculator;
    private Map<Measurement, String> map;

    @FXML
    public Label inputLabel;

    @FXML
    public TextArea inputArea;

    @FXML
    public Label unitsLabel;

    @FXML
    public TextField unitsField;

    @FXML
    public Label outputLabel;

    @FXML
    public TextArea outputArea;

    @FXML
    public Button btn;

    public Controller(Calculator calculator, Map<Measurement, String> map) {
        this.calculator = calculator;
        this.map = map;
    }

    public void calculate(ActionEvent actionEvent) {
        String inputText = inputArea.getText();
        String unitsText = unitsField.getText();
        String outputText = "";
        try {
            Unit inputUnit = calculator.calculateAll(inputText);
            Unit answerUnit;
            Measurement inputMeasurement = new Measurement(inputUnit.getMeasurement());

            if (!unitsText.equals("")) {
                answerUnit = calculator.calculateAll(unitsText);
                Measurement answerMeasurement = answerUnit.getMeasurement();
                if (inputMeasurement.equals(answerMeasurement)) {
                    inputUnit.div(answerUnit);
                } else {
                    throw new InvalidArithmeticException(inputMeasurement +
                            " is not equivalent of " +  answerMeasurement);
                }
            }

            String measurementName = map.get(inputMeasurement);
            outputText = inputUnit.getValue() + " which is measurement of type: " + measurementName;
        } catch (InvalidArithmeticException | InvalidSyntaxException e) {
            outputText = e.getMessage();
        } finally {
            outputArea.setText(outputText);
        }
    }
}
