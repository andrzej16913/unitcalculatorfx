package main.java.util.calculatorLogic;

public class FlowModifier extends Word {
    FlowModifier(String name) {
        super(name);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj instanceof FlowModifier) {
            return getName().equals(((FlowModifier) obj).getName());
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return super.getName().hashCode();
    }

    @Override
    public String toString() {
        return "FlowModifier: '" + super.getName() + "'";
    }
}
