package main.java.util.calculatorLogic.exceptions;

public class InvalidSyntaxException extends Exception {

    public InvalidSyntaxException() {
        super();
    }

    public InvalidSyntaxException(String message) {
        super(message);
    }
}
