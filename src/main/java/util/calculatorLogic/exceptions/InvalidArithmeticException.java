package main.java.util.calculatorLogic.exceptions;

public class InvalidArithmeticException extends Exception {

    public InvalidArithmeticException() {
        super();
    }

    public InvalidArithmeticException(String message) {
        super(message);
    }
}
