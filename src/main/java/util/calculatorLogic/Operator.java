package main.java.util.calculatorLogic;

import main.java.util.calculatorLogic.exceptions.InvalidArithmeticException;

public class Operator extends Word {

    private final int force;
    private final boolean left;
    private final boolean right;
    private ThrowingBiConsumer<Unit, Unit> operation;

    public Operator(String name, int force, boolean left, boolean right, ThrowingBiConsumer<Unit, Unit> op) {
        super(name);
        this.force = force;
        this.left = left;
        this.right = right;
        operation = op;
    }

    @Override
    public String toString() {
        return "Operator: " + getName();
    }

    public int getForce() {
        return force;
    }

    public boolean isLeft() {
        return left;
    }

    public boolean isRight() {
        return right;
    }

    public void calculate(Unit upgraded, Unit value) throws InvalidArithmeticException {
        operation.accept(upgraded, value);
    }
}
