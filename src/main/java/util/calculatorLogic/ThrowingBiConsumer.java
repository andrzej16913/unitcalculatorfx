package main.java.util.calculatorLogic;

import main.java.util.calculatorLogic.exceptions.InvalidArithmeticException;

@FunctionalInterface
public interface ThrowingBiConsumer<R, T> {
    void accept(R r, T t) throws InvalidArithmeticException;
}
