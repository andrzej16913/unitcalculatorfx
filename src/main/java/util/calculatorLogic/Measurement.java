package main.java.util.calculatorLogic;

public class Measurement {

    public static final int NUMBER_OF_BASE_UNITS = 7;
    public static final int BITS_PER_POWER = 4;
    public static final String[] UNIT_SYMBOLS = {
            "A", "cd", "K", "kg", "m", "mol", "s"
    };

    private byte[] powers;
    private final String name;

    public Measurement (byte[] powers, String name) {
        if (powers == null) {
            throw new NullPointerException();
        }
        if (powers.length < NUMBER_OF_BASE_UNITS) {
            throw new IndexOutOfBoundsException();
        }
        this.powers = powers.clone();
        this.name = name;
    }

    public Measurement (byte[] powers) {
        this(powers, null);
    }

    public Measurement (String name) {
        this.name = name;
        powers = new byte[NUMBER_OF_BASE_UNITS];
        for (int i = 0; i < NUMBER_OF_BASE_UNITS; i++) {
            powers[i] = 0;
        }
    }
	
	public Measurement () {
		this.name = null;
        powers = new byte[NUMBER_OF_BASE_UNITS];
        for (int i = 0; i < NUMBER_OF_BASE_UNITS; i++) {
            powers[i] = 0;
        }
	}
	
	public Measurement(Measurement other) {
		name = other.getName();
		powers = other.getPowers().clone();
	}

    public byte[] getPowers() {
        return powers;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;

        if (!(obj instanceof Measurement))
            return false;

        if (obj == this)
            return true;

        byte[] otherPowers = ((Measurement)obj).getPowers();
        for (int i = 0; i < NUMBER_OF_BASE_UNITS; i++) {
            if (powers[i] != otherPowers[i]) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        for (byte b: powers) {
            b <<= BITS_PER_POWER;
            b >>>= BITS_PER_POWER;
            hash += b;
            hash <<= BITS_PER_POWER;
        }
        return hash;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("1");

        for (int i = 0; i < NUMBER_OF_BASE_UNITS; i++) {
            if (powers[i] < 0) {
                result.append(UNIT_SYMBOLS[i]);
                result.append("^(");
                result.append(powers[i]);
                result.append(")*");
            } else if (powers[i] > 0) {
                result.append(UNIT_SYMBOLS[i]);
                if (powers[i] > 1) {
                    result.append("^");
                    result.append(powers[i]);
                }
                result.append("*");
            }
        }

        if (result.charAt(result.length() - 1) == '*') {
            result.deleteCharAt(result.length() - 1);
        }

        if (result.length() > 1 && result.charAt(0) == '1') {
            result.deleteCharAt(0);
        }

        if (name != null) {
            result.insert(0, name + ":\n");
        }

        return result.toString();
    }

    public void multiply(Measurement other) {
        byte[] otherBytes = other.getPowers();
        for (int i = 0; i < NUMBER_OF_BASE_UNITS; i++) {
            powers[i] += otherBytes[i];
        }
    }

    public void divide(Measurement other) {
        byte[] otherBytes = other.getPowers();
        for (int i = 0; i < NUMBER_OF_BASE_UNITS; i++) {
            powers[i] -= otherBytes[i];
        }
    }
}
