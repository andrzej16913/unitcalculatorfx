package main.java.util.calculatorLogic;

//import logic.Measurement;

import main.java.util.calculatorLogic.exceptions.InvalidArithmeticException;

public class Unit extends Word {

    private double value;
    private Measurement measurement;

    public Unit(String name, double value, Measurement measurement) {
        super(name);
        this.value = value;
        this.measurement = measurement;
    }

    public Unit(Unit other) {
        super(other.getName());
        value = other.getValue();
        measurement = new Measurement(other.getMeasurement().getPowers());
    }

    public Unit(String name, Measurement measurement) {
        this(name, 1, measurement);
    }

    public Unit(double value, Measurement measurement) {
        this(null, value, measurement);
    }

    public Unit(double value) {
        this(null, value, new Measurement());
    }

    public Unit(Measurement measurement) {
        this(null, 1, measurement);
    }

    public Unit() {
        this(null, 1, new Measurement());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;

        if (!(obj instanceof Unit))
            return false;

        if (obj == this)
            return true;

        if (value != ((Unit) obj).getValue()) {
            return false;
        }

        return measurement == ((Unit) obj).getMeasurement();
    }

    @Override
    public int hashCode() {
        long v = Double.doubleToLongBits(value);
        int x = (int)(v^(v>>>32));
        return x^measurement.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        if (getName() != null) {
            result.append(getName());
            result.append(" ,");
        }

        result.append("value = ");
        result.append(value);
        result.append("\nmeasurement: ");
        result.append(measurement.toString());

        return result.toString();
    }

    public double getValue() {
        return value;
    }

    public Measurement getMeasurement() {
        return measurement;
    }

    public void add(Unit unit) throws InvalidArithmeticException {
        if (!measurement.equals(unit.getMeasurement())) {
            throw new InvalidArithmeticException(measurement.toString() + " is not equivalent of " +
                    unit.getMeasurement().toString());
        } else {
            value += unit.getValue();
        }
    }

    public void sub(Unit unit) throws InvalidArithmeticException {
        if (!measurement.equals(unit.getMeasurement())) {
            throw new InvalidArithmeticException(measurement.toString() + " is not equivalent of " +
                    unit.getMeasurement().toString());
        } else {
            value -= unit.getValue();
        }
    }

    public void mul(Unit unit) {
        value *= unit.getValue();
        measurement.multiply(unit.getMeasurement());
    }

    public void div(Unit unit) throws InvalidArithmeticException {
        if (unit.getValue() == 0) {
            throw new InvalidArithmeticException("You cannot divide by 0!");
        }
        value /= unit.getValue();
        measurement.divide(unit.getMeasurement());
    }
}
