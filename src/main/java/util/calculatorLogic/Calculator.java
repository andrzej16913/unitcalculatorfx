package main.java.util.calculatorLogic;

import main.java.util.calculatorLogic.exceptions.InvalidArithmeticException;
import main.java.util.calculatorLogic.exceptions.InvalidSyntaxException;

import java.util.*;

public class Calculator {

    private final Map<String, Word> dictionary;
    private final int MAX_WORD_LENGTH;

    public Calculator(Map<String, Word> dict, int wordLength) {
        dict.put("+", new Operator("+", 0, true, true, Unit::add));
        dict.put("-", new Operator("-", 0, true, false, Unit::sub));
        dict.put("*", new Operator("*", 1, true, true, Unit::mul));
        dict.put("/", new Operator("/", 1, true, false, Unit::div));
        dictionary = dict;
        MAX_WORD_LENGTH = wordLength;
    }

    public Unit calculateAll(String input) throws InvalidArithmeticException, InvalidSyntaxException {
        List<String> list = inputAsList(input);
        list = fillWithMuls(list);
        List<Word> words = listToWords(list);
        words = asReversedPolishNotation(words);
        return finalCalculation(words);
    }

    private boolean isOperator(char letter) {
        if (letter == '-') return true;
        if (letter == '+') return true;
        if (letter == '*') return true;
        return letter == '/';
    }

    private boolean isFlowModifier(char letter) {
        return letter == '(' || letter == ')';
    }

    private boolean checkLetter(char letter) {
        if (Character.isLetter(letter)) return true;
        if (Character.isDigit(letter)) return true;
        if (letter == '.') return true;
        return isOperator(letter);
    }

    private int isSplittable(char a, char b) {
        if (Character.isDigit(a)) {
            if (Character.isDigit(b)) return 0;
            if (Character.isLetter(b)) return 0;
            if (b == '.') return 0;
            if (isFlowModifier(b)) return 1;
            return 1;
        }

        if (Character.isLetter(a)) {
            if (Character.isLetter(b)) return 0;
            if (b == '.') return -1;
            if (isFlowModifier(b)) return 1;
            return 1;
        }

        if (isOperator(a)) {
            if (Character.isLetter(b)) return 1;
            if (Character.isDigit(b)) return 1;
            if (b == '(') return 1;
            if (b == ')') return -1;
            if (isOperator(b)) return -1;
            if (b == '.') return -1;
        }

        if (a == '.') {
            if (Character.isDigit(b)) return 0;
            return -1;
        }

        if (isFlowModifier(a)) {
            if (Character.isDigit(b)) return 1;
            if (Character.isLetter(b)) return 1;
            if (b == '.') return -1;
            if (isOperator(b)) {
                if (a == ')') return 1;
                else return -1;
            }
        }

        return 0;
    }

    private int countDots(String s) {
        int dotCounter = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '.') {
                dotCounter++;
            }
        }
        return dotCounter;
    }

    public List<String> fillWithMuls(List<String> list) {
        ListIterator<String> it = list.listIterator();
        String a = it.next();
        String b;
        while (it.hasNext()) {
            b = it.next();
            it.previous();
            if (!isOperator(a.charAt(0)) && !isOperator(b.charAt(0))) {
                it.add("*");
            }
            a = it.next();
        }
        return list;
    }

    public List<String> inputAsList(String equation) throws InvalidSyntaxException {
        List<String> list = new LinkedList<>();
        equation = equation.trim();
        String[] tmp = equation.split("\\s");
        StringBuilder bld = new StringBuilder();
        for (String s: tmp) {
            bld.append(s);
        }
        equation = bld.toString();
        int lastSplitIndex = 0;

        if (!checkLetter(equation.charAt(0))) {
            throw new InvalidSyntaxException("\'" + equation.charAt(0) + "\' is not a valid character!");
        }

        for (int i = 1; i < equation.length(); i++) {
            char x = equation.charAt(i-1);
            char y = equation.charAt(i);
            if (!checkLetter(y)) {
                throw new InvalidSyntaxException("\'" + y + "\' is not a valid character!");
            }

            switch (isSplittable(x, y)) {
                case 1:
                    list.add(equation.substring(lastSplitIndex, i));
                    lastSplitIndex = i;
                    break;

                case -1:
                    throw new InvalidSyntaxException("\'" + x + "\' cannot be followed by \'" + y + "\'");
            }
        }

        list.add(equation.substring(lastSplitIndex));

        return list;
    }

    private String stringFromArray(String[] array) {
        StringBuilder result = new StringBuilder();
        for (String s: array) {
            result.append(s);
        }
        return result.toString();
    }

    private Unit parseUnit(String symbols, Unit unit) throws InvalidSyntaxException {
        int pos = 0;
        int i = 1;
        byte[] bytes = {0, 0, 0, 0, 0, 0, 0};

        if (symbols.contains("m")) {
            if (symbols.contains("mol")) {
                symbols = stringFromArray(symbols.split("mol"));
                bytes[5] = 1;
                unit.mul(new Unit(new Measurement(bytes)));
                bytes[5] = 0;
            }

            if (symbols.endsWith("m")) {
                symbols = symbols.substring(0, symbols.length() - 1);
                bytes[4] = 1;
                unit.mul(new Unit(new Measurement(bytes)));
                bytes[4] = 0;
            }

            if (symbols.startsWith("mi")) {
                symbols = symbols.substring(2);
                unit.mul(new Unit(0.000001));
            }

            if (symbols.startsWith("m")) {
                symbols = symbols.substring(1);
                unit.mul(new Unit(0.001));
            }
        }

        if (symbols.startsWith("da")) {
            symbols = symbols.substring(2);
            unit.mul(new Unit(10));
        }

        if (symbols.length() == 0) {
            return unit;
        }

        String statement = symbols.substring(0, 1);

        while (statement.length() <= MAX_WORD_LENGTH && i <= symbols.length()) {
            if (dictionary.containsKey(statement)) {
                Word w = dictionary.get(statement);
                if (w instanceof Unit) {
                    unit.mul((Unit) w);
                    pos = i;
                } else {
                    throw new InvalidSyntaxException("'" + statement + "' is not an Unit!");
                }
            }
            i++;
            if (i <= symbols.length()) {
                statement = symbols.substring(pos, i);
            } else if (pos + 1 < i) {
                throw new InvalidSyntaxException(statement + " is not a statement");
            }
        }
        return unit;
    }

    public List<Word> listToWords(List<String> stringList) throws InvalidSyntaxException {
        List<Word> words = new LinkedList<>();

        for (String s: stringList) {
            if (s.length() == 1 && isOperator(s.charAt(0))) {
                words.add(dictionary.get(s));
            } else {
                if (countDots(s) > 1) {
                    throw new InvalidSyntaxException("Too much dots!");
                }

                String[] num = s.split("[A-Za-z]");
                if (num.length > 1) {
                    throw new InvalidSyntaxException("num is larger than 1!");
                }

                Unit unit;
                if (num.length == 1) {
                    unit = new Unit(Double.parseDouble(num[0]));
                } else {
                    unit = new Unit();
                }

                if (num.length == 0) {
                    //String symbols = ;
                    words.add(parseUnit(s, unit));
                } else if (!s.equals(num[0])) {
                    words.add(parseUnit(s.substring(num[0].length()), unit));
                } else {
                    words.add(unit);
                }
            }
        }

        return words;
    }

    public List<Word> asReversedPolishNotation(List<Word> input) {
        List<Word> output = new LinkedList<>();
        LinkedList<Operator> stack = new LinkedList<>();

        for (Word w: input) {
            if (w instanceof Unit) {
                output.add(w);
            } else if (w instanceof Operator) {
                Operator o1 = (Operator) w;
                if (!stack.isEmpty()) {
                    Operator o2 = stack.getLast();
                    while ((o2.isLeft() && o1.getForce() <= o2.getForce()) ||
                            (o2.isRight() && o1.getForce() < o2.getForce())) {

                        output.add(o2);
                        stack.removeLast();
                        if (stack.isEmpty()) break;
                        o2 = stack.getLast();
                    }
                }
                stack.add(o1);
            }
        }

        while (!stack.isEmpty()) {
            output.add(stack.removeLast());
        }

        return output;
    }

    public Unit finalCalculation(List<Word> words) throws InvalidArithmeticException, InvalidSyntaxException {
        LinkedList<Unit> stack = new LinkedList<>();
        //Unit result = new Unit();

        for (Word w: words) {
            if (w instanceof Unit) {
                stack.add((Unit) w);
            }
            if (w instanceof Operator) {
                try {
                    Unit u2 = stack.removeLast();
                    Unit u1 = stack.removeLast();
                    ((Operator) w).calculate(u1, u2);
                    stack.add(u1);
                } catch (NoSuchElementException e) {
                    throw new InvalidSyntaxException("Not enough Units on the stack");
                }
            }
        }

        if (stack.size() != 1) {
            throw new InvalidSyntaxException("Too much Units");
        }

        return stack.getFirst();
    }
}
