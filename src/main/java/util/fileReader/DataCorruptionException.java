package main.java.util.fileReader;

public class DataCorruptionException extends Exception {
	public DataCorruptionException() {
		super();
	}
	
	public DataCorruptionException(String message) {
		super(message);
	}
}