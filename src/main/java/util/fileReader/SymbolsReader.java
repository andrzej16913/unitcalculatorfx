package main.java.util.fileReader;

import java.io.File;
import java.net.URL;
import java.util.Locale;
import java.util.Scanner;
import java.util.Map;
import java.util.HashMap;
import java.io.*;
import javafx.util.Pair;
import main.java.util.calculatorLogic.Measurement;
import main.java.util.calculatorLogic.Unit;
import main.java.util.calculatorLogic.Word;

public class SymbolsReader {
	public static Pair<Map<String, Measurement>, Map<Measurement, String> > readMeasurements(String path)
		throws FileNotFoundException, DataCorruptionException {
			
		Map<String, Measurement> map = new HashMap<>();
		Map<Measurement, String> reversedMap = new HashMap<>();
		URL url = SymbolsReader.class.getResource(path);
		File file = new File(url.getPath());
		Scanner sc = new Scanner(file);

		final Measurement q = new Measurement("quantity");
		map.put("quantity", q);
		reversedMap.put(q, "quantity");

		Measurement[] measurements = new Measurement[Measurement.NUMBER_OF_BASE_UNITS];
		String[] names = {
			"amperage",
			"luminousIntensity",
			"temperature",
			"mass",
			"distance",
			"amountOfSubstance",
			"time"
		};
		
		for (int i = 0; i < Measurement.NUMBER_OF_BASE_UNITS; i++) {
			measurements[i] = new Measurement(powersGenerator(i), names[i]);
			map.put(names[i], measurements[i]);
			reversedMap.put(measurements[i], names[i]);
		}
		
		String word;
		String measurement;
		Measurement m = null;
		while(sc.hasNext()) {
			word = sc.next();
			if (word.equals("*") || word.equals("/")) {
				if (sc.hasNext()) {
					measurement = sc.next();
					if (!map.containsKey(measurement)) {
						throw new DataCorruptionException("Invalid measurement: " + measurement);
					}

					if (m == null) {
						throw new DataCorruptionException(word + " without initial measurement");
					}
					
					if (word.equals("*")) {
						m.multiply(map.get(measurement));
					} else {
						m.divide(map.get(measurement));
					}
					
				} else {
					throw new DataCorruptionException(word + " not followed by measurement");
				}
			} else {
				if (m != null) {
					map.put(m.getName(), m);
					reversedMap.put(m, m.getName());
				}
				m = new Measurement(word);
			}
		}

		if (m != null) {
			map.put(m.getName(), m);
			reversedMap.put(m, m.getName());
		}
		
		return new Pair<>(map, reversedMap);
	}
	
	private static byte[] powersGenerator(int x) {
		byte[] bytes = new byte[Measurement.NUMBER_OF_BASE_UNITS];
		for (int i = 0; i < Measurement.NUMBER_OF_BASE_UNITS; i++) {
			if (i == x) {
				bytes[i] = 1;
			} else {
				bytes[i] = 0;
			}
		}
		return bytes;
	}

	public static Map<String, Word> readUnits(String path, Map<String, Measurement> dictionary)
		throws FileNotFoundException, DataCorruptionException {

		Map<String, Word> output = new HashMap<>();
		URL url = SymbolsReader.class.getResource(path);
		File file = new File(url.getPath());
		Scanner sc = new Scanner(file);
		sc.useLocale(Locale.US);

		String unit;
		String measurement;
		double value;

		while (sc.hasNext()) {
			unit = sc.next();

			if (sc.hasNext()) {
				measurement = sc.next();
			} else {
				throw new DataCorruptionException("Unit name not followed by its measurement");
			}

			if (!dictionary.containsKey(measurement)) {
				throw new DataCorruptionException("Invalid measurement name: " + measurement);
			}

			final Unit u;

			if (sc.hasNextDouble()) {
				value = sc.nextDouble();
				u = new Unit(unit, value, dictionary.get(measurement));
			} else {
				u = new Unit(unit, dictionary.get(measurement));
			}

			output.put(unit, u);
		}

		return output;
	}
}