package main.java.util.fileReader;

public class Pair<T, R> {
	
	private T t;
	private R r;
	
	Pair(T a, R b) {
		t = a;
		r = b;
	}
	
	public T getFirst() {
		return t;
	}
	
	public R getSecond() {
		return r;
	}
}